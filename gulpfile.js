var lr         = require('tiny-lr'), // Минивебсервер для livereload
    watch      = require('gulp-watch'),
    gulp       = require('gulp'), // Сообственно Gulp JS
    livereload = require('gulp-livereload'), // Livereload для Gulp
    rigger     = require('gulp-rigger'),
    concat     = require('gulp-concat'), // Склейка файлов
    plumber    = require('gulp-plumber'),
    rimraf     = require('rimraf'),
    server     = lr();

var
	__path = {
		build:{ //Тут мы укажем куда складывать готовые после сборки файлы
			html     :'build/',
			js       :'build/js/',
			css      :'build/css/',
			img      :'build/i/',
			bg_images:'build/i/bg/',
			sprite   :'build/i/bg/',
			fonts    :'build/fonts/'
		},
		src  :{ //Пути откуда брать исходники
			html  :'src/*.html', //Синтаксис src/*.html говорит gulp что мы хотим взять все файлы с расширением .html
			js    :'src/js/script.js',//В стилях и скриптах нам понадобятся только main файлы
			style :'src/style/style.less',
			img   :['./src/i/**/*.*', '!./src/i/sprite/**/*.*'], //Синтаксис img/**/*.* означает - взять все файлы всех расширений из папки и из вложенных каталогов
			sprite:'src/i/sprite/**/*.*',
			fonts :'src/fonts/**/*.*'
		},
		watch:{ //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
			html  :'src/**/*.html',
			js    :'src/js/**/*',
			style :'src/style/**/*.less',
			img   :['src/i/**/*.*', '!./src/i/sprite/**/*.*'],
			sprite:'src/i/sprite/**/*.*',
			fonts :'src/fonts/**/*.*'
		},
		clean:'./build'
	};

// HTML
gulp.task('html', function()
{
	gulp.src(__path.src.html) //Выберем файлы по нужному пути
		.pipe(plumber())
		.pipe(rigger()) //Прогоним через rigger
		.pipe(gulp.dest(__path.build.html)) //Выплюнем их в папку build
		.pipe(livereload(server)); //И перезагрузим наш сервер для обновлений
});

// LESS
gulp.task('style', function()
{

	var path                 = require('path'),
	    less                 = require('gulp-less'), // Плагин для Less
	    LessPluginAutoPrefix = require('less-plugin-autoprefix'),
	    autoprefix           = new LessPluginAutoPrefix({browsers:["last 3 versions"]});
	console.log('less begin');
	return gulp.src(__path.src.style)
		.pipe(plumber())
		.pipe(less({
			paths  :[path.join(__dirname, 'less')],
			plugins:[autoprefix]
		}))
		//		.pipe(cssmin()) //Сожмем
		.pipe(gulp.dest(__path.build.css)) //И в build
		.pipe(livereload(server)); // даем команду на перезагрузку css
});

// JAVASCRIPT
gulp.task('js', function()
{
	var uglify = require('gulp-uglify'); // Минификация JS
	gulp.src(__path.src.js) //Найдем наш main файл
		.pipe(plumber())
		.pipe(rigger()) //Прогоним через rigger
		//.pipe(uglify()) //Сожмем наш js
		.pipe(gulp.dest(__path.build.js)) //Выплюнем готовый файл в build
		.pipe(livereload(server)); // даем команду на перезагрузку страницы
});

// IMAGES MIN
gulp.task('images', function()
{
	var changed = require('gulp-changed');
	gulp.src(__path.src.img) //Выберем наши картинки
		.pipe(plumber())
		.pipe(changed(__path.build.img))
		.pipe(gulp.dest(__path.build.img)) //И бросим в build
		.pipe(livereload(server));
	return;

	var imagemin = require('gulp-imagemin'), // Минификация изображений
	    pngquant = require('imagemin-pngquant');
	gulp.src(__path.src.img) //Выберем наши картинки
		.pipe(plumber())
		.pipe(imagemin({ //Сожмем их
			progressive:true,
			svgoPlugins:[{removeViewBox:false}],
			use        :[pngquant()],
			interlaced :true
		}))
		.pipe(gulp.dest(__path.build.img)) //И бросим в build
		.pipe(livereload(server));

});

// FONTS
gulp.task('fonts', function()
{
	gulp.src(__path.src.fonts)
		.pipe(gulp.dest(__path.build.fonts))
});

// HTTP SERVER
gulp.task('http-server', function()
{
	var connect       = require('connect'),
	    connectStatic = require('serve-static');
	connect()
		.use(require('connect-livereload')())
		.use(connectStatic('./build'))
		.listen('9000');
	console.log('Server listening on http://localhost:9000');
});

// BOWER COMPONENTS
gulp.task('bower', function()
{
	var uglify = require('gulp-uglify'); // Минификация JS
	var arJs = [
		'./bower_components/jquery/dist/jquery.min.js',
	];
	gulp.src(arJs)
		.pipe(uglify())
		.pipe(gulp.dest(__path.build.js));
});

gulp.task('sprite', function()
{
	var spritesmith = require('gulp.spritesmith'),
	    spriteData  =
		    gulp.src(__path.src.sprite) // путь, откуда берем картинки для спрайта
			    .pipe(spritesmith({
				    imgName    :'sprite.png',
				    cssName    :'sprite.less',
				    algorithm  :'top-down',//'binary-tree',
				    imgPath    :'../i/bg/sprite.png',
				    cssTemplate:'less.template.spritesmith',
				    cssVarMap  :function(sprite)
				    {
					    sprite.name = 'icon-' + sprite.name
				    }
			    }));

	spriteData.img.pipe(gulp.dest(__path.build.sprite)); // путь, куда сохраняем картинку
	spriteData.css.pipe(gulp.dest('./src/style/lib')); // путь, куда сохраняем стили
});

gulp.task('watch', function()
{
	// Подключаем Livereload
	livereload.listen(35729, function(err)
	{
		if(err)
		{
			return console.log(err);
		}
		gulp.watch([__path.watch.html], function(event, cb)
		{
			gulp.start('html');
			console.log('html:change', event)
		});
		gulp.watch([__path.watch.style], function(event, cb)
		{
			gulp.start('style');
			console.log('style:change', event)
		});
		gulp.watch([__path.watch.js], function(event, cb)
		{
			gulp.start('js');
			console.log('js:change', event)
		});
	});
});

gulp.task('build', [
	'sprite',
	'html',
	'js',
	'style',
	'fonts',
	'images',
	'bower'
]);

gulp.task('clean', function(cb)
{
	rimraf(__path.clean, cb);
});

gulp.task('default', ['build', 'watch', 'http-server']);
gulp.task('chrome', ['build', 'watch', 'http-server'], function()
{
	var cp = require('child_process');

	cp.exec('"C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe" http://localhost:9000', function()
	{
		console.log('Runing in browser');
	});
});
